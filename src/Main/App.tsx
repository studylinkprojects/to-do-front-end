import React from 'react';
import './App.css';
import {TodoView} from "../Presentation/TodoView";
import {TodoDataSourceImpl} from "../Data/DataSource/TodoDataSourceApi/TodoDataSourceImpl";
import {TodoRepositoryImpl} from "../Data/RepositoryImpl/TodoRepositoryImpl";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import {store} from "../Data/redux/store";

function App() {
  const datasource = new TodoDataSourceImpl()
  const repository  = new TodoRepositoryImpl(datasource)

  return (
    <div className="App">
        <Provider store={store}>
          <BrowserRouter>
                <TodoView repository={repository}/>
          </BrowserRouter>
        </Provider>
    </div>
  );
}

export default App;
