import React, {useContext, useState} from "react";
import {Button, Container, Form, FormGroup, Input, Label} from "reactstrap";
import logo from "../../assets/Images/logo.png";
import {Link} from "react-router-dom";
import './login.css'

export const Login:React.FC = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    return(
        <>
            <Container>
                <div className='login-container'>
                    <div className="login-card p-4 m-auto mt-5 w-40">
                        <div className="logo">
                            <img src={logo} alt=""/>
                        </div>
                        <Form className='mt-5'>
                            <FormGroup>
                                <Label className='float-start' htmlFor='username'>Email</Label>
                                <Input type='email' name='username' onChange={(e) => {
                                    setEmail(e.target.value)
                                }
                                }/>
                            </FormGroup>
                            <FormGroup>
                                <Label className='float-start' htmlFor='password'>Password</Label>
                                <Input type='password' name='password' onChange={(e) => {
                                    setPassword(e.target.value)
                                }
                                }/>
                            </FormGroup>
                            <Button className='btn-primary mt-3 w-100'>
                                Login
                            </Button>
                        </Form>
                        <div className='mt-3'>
                            <span>or <Link to='/register' className='text-primary text-decoration-none'>Register</Link></span>
                        </div>
                    </div>
                </div>
            </Container>
        </>

    )
}