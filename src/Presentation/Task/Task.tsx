import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import React from "react";
import {useNavigate} from "react-router-dom";
import './task.css'
import { FiEdit, FiTrash2 } from "react-icons/fi";
import {useTodoModelController} from "../hooks/useTodoModelController";
import {TodoRepository} from "../../Domain/Repository/Repository";


export type TaskProp = {
  id: number,
  name: string,
  status: boolean,
    repository: TodoRepository

}
export const Task:React.FC<TaskProp> = ({id, name, status, repository}) => {
  const navigate = useNavigate();
  const {handleDeleteTask, handleMarkAsComplete} = useTodoModelController(repository)
  return (
      <div className={`task d-flex flex-row align-items-center justify-content-between mb-3 ${status ? 'w-100':'w-75'}`}>
        <Form>
          <FormGroup check className='m-0' >
            <Input type="checkbox"
                   htmlFor='chk'
                   onChange={() => handleMarkAsComplete(id)}
                   disabled={status}
                   checked={status}/>
            <Label name='chk' className={`fs-6 text-color mb-0 ${status? 'text-decoration-line-through': ''}`}>
              {name}
            </Label>
          </FormGroup>
        </Form>
        { !status &&
            <div>
              <Button color='outline' className='action-buttons' onClick={() => {
                navigate('/editTask',{state:{
                    id:id,
                    name:name
                  }})
              }}>
                <FiEdit/>
              </Button>
              <Button color='outline'
                      className='action-buttons'
                      onClick={() => handleDeleteTask(id)}>
                <FiTrash2/>
              </Button>
            </div>
        }
      </div>
  );
}