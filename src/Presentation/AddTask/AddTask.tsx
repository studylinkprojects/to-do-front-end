
import {Button, Form, FormGroup, Input, Label} from "reactstrap"
import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import {TodoRepository} from "../../Domain/Repository/Repository";
import {useTodoModelController} from "../hooks/useTodoModelController";
type AddTaskProps = {
    repository: TodoRepository
}

export const AddTask:React.FC<AddTaskProps> = ({repository}) => {
    const [name, setName] = useState('');
    const navigate = useNavigate()
    const {handleAddTask} = useTodoModelController(repository)

    return(
        <div className='mt-4 mb-4 card w-50 m-auto p-3'>
            <h3 className="text-color">Add Task</h3>
            <Form className='mt-2'>
                <FormGroup>
                    <Label className='float-start' htmlFor='taskName'>Name</Label>
                    <Input type='text' name='taskName' autoFocus={true} onChange={(e) => {
                        setName(e.target.value)
                    }
                    }/>
                </FormGroup>
                <div>
                    <div className='float-end'>
                        <Button onClick={() => navigate('/')}
                                className='btn-secondary mt-2 me-2'>
                            Cancel
                        </Button>
                        <Button disabled={name?.length <= 0}
                                className='btn-primary mt-2'
                                type='submit'
                                onClick={()=> handleAddTask( name) }
                        >
                            Add Task
                        </Button>
                    </div>
                </div>
            </Form>
        </div>
    )
}