import React, {useEffect, useState} from "react";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {useLocation, useNavigate} from "react-router-dom";
import {TodoRepository} from "../../Domain/Repository/Repository";
import {useTodoModelController} from "../hooks/useTodoModelController";

type EditTaskProp = {
    repository: TodoRepository
}

export const EditTask:React.FC<EditTaskProp> = ({repository}) => {
   const location = useLocation()
    const {id, name} = location.state;
    const [taskName, setTaskName] = useState(name);
    const navigate = useNavigate();
    const {handleEditTask} = useTodoModelController(repository);

    return <div className='mt-4 mb-4 card w-50 m-auto p-3'>
        <h3 className="text-color">Edit Task</h3>
        <Form className='mt-2'>
            <FormGroup>
                <Label className='float-start' htmlFor='taskName'>Name</Label>
                <Input type='text' name='taskName' value={taskName} onChange={(e) => {
                    setTaskName(e.target.value)
                }
                }/>
            </FormGroup>
            <div>
                <div className='float-end'>
                    <Button onClick={() => navigate('/')}
                            className='btn-secondary mt-2 me-2'>
                        Cancel
                    </Button>
                    <Button
                        disabled={ !(name && name.length > 0)}
                        className='btn-primary mt-2'
                        type='submit'
                        onClick={() => handleEditTask(name, id)}
                    >
                        Edit Task
                    </Button>
                </div>
            </div>
        </Form>
    </div>

}