import React, {useState} from "react";
import {Button, Container, Form, FormGroup, Input, Label} from "reactstrap";
import logo from '../../assets/Images/logo.png'
import './register.css'
import {Link} from "react-router-dom";

export const Register:React.FC = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
  return(
      <>
          <Container>
              <div className='register-container'>
                <div className="register-card p-4 m-auto mt-5 border-3 w-40">
                    <div className="logo">
                        <img src={logo} alt=""/>
                    </div>
                    <Form className='mt-5'>
                        <FormGroup>
                            <Label className='float-start' htmlFor='username'>Name</Label>
                            <Input type='text' name='username' value={name} autoFocus={true} onChange={(e) => {
                                setName(e.target.value)
                            }
                            }/>
                        </FormGroup>
                        <FormGroup>
                            <Label className='float-start' htmlFor='email'>Email</Label>
                            <Input type='email' name='email' value={email} onChange={(e) => {
                                setEmail(e.target.value)
                            }
                            }/>
                        </FormGroup>
                        <FormGroup>
                            <Label className='float-start' htmlFor='password'>Password</Label>
                            <Input type='password' value={password} name='password' onChange={(e) => {
                                setPassword(e.target.value)
                            }
                            }/>
                        </FormGroup>
                        <Button className='btn-primary mt-3 w-100'>
                            Register
                        </Button>
                    </Form>
                    <div className='mt-3'>
                        <span>or <Link to='/login' className='text-primary text-decoration-none'>Login</Link></span>
                    </div>
                </div>
              </div>
          </Container>
      </>
  )
}