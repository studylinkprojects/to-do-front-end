import {Col} from "reactstrap";
import logo from '../../assets/Images/logo.png'
import { FaRegListAlt } from "react-icons/fa";
import './sidebar.css'

export const Sidebar  = () => {
    return (
        <Col xs='2'>
            <div className="sidebar">
                <div className="logo d-flex align-items-center justify-content-center my-2">
                    <img src={logo} alt='Logo'/>
                </div>
                <div className="sidebar-items-list px-3 my-2">
                    <div className="sidebar-item mt-3 d-flex align-items-center justify-content-center gap-2 py-2 m-1">
                        <FaRegListAlt className='text-grey'/>
                        <span className='text-grey'>My tasks</span>
                    </div>
                </div>
            </div>
        </Col>
    )
}