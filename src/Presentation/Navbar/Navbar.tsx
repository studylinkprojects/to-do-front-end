import './navbar.css'
import { FiChevronDown } from "react-icons/fi";
export const Navbar = () => {
  return (
      <div className='navbar d-flex justify-content-end gap-2 mt-3 p-3 align-items-center'>
        <div className="profile-name">
          John Doe
        </div>
        <div>
          <FiChevronDown/>
        </div>
      </div>
  )
}