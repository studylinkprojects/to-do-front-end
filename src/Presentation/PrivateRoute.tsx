import React from "react";
import {useAppSelector} from "../Data/redux/hooks";
import {Navigate} from "react-router-dom";


type PrivateRouteProps = {
    children: JSX.Element
}
export const PrivateRoute:React.FC<PrivateRouteProps> = ({children}) => {
    const isLoggedIn = useAppSelector(state => state.login.isLoggedIn)
    return (isLoggedIn ? <>{children}</> : <Navigate to='/login'/>)
}