import { Outlet } from "react-router-dom";
import {Col, Container, Row} from "reactstrap";
import { Navbar } from "../Navbar/Navbar";
import { Sidebar } from "../SideBar/Sidebar";
import React from "react";


export const SharedLayout:React.FC = () => {
    return (
        <Container fluid>
            <Row>
                <Sidebar/>
                <Col xs='10'>
                    <Navbar/>
                    <Outlet/>
                </Col>
            </Row>
        </Container>
    )
}