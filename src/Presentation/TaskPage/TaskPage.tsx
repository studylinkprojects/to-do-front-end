import { FiPlus } from "react-icons/fi";
import {AccordionBody, AccordionHeader, AccordionItem, Button, UncontrolledAccordion } from "reactstrap";
import {useTodoModelController} from "../hooks/useTodoModelController";
import React, {useEffect} from "react";
import {TodoRepository} from "../../Domain/Repository/Repository";
import { Todo } from "../../Domain/Model/Model";
import {Task} from "../Task/Task";
import {useNavigate} from "react-router-dom";
import './taskpage.css'

type TaskPageProps = {
    repository: TodoRepository
}

export const TaskPage:React.FC<TaskPageProps> = ({repository}) => {
    const {todoList, init} = useTodoModelController(repository)
    const navigate = useNavigate()
    const pendingTasks = todoList?.filter((todo: Todo) => !todo.status) ?? null
    const completedTasks = todoList?.filter((todo: Todo) => todo.status) ?? null

    useEffect(() => {
        init()
    }, []);

  return(
      <>
          <div className='title'>
              <h3 className='text-xl-start text-grey mt-3'>My Tasks</h3>
          </div>
          <Button className='btn-primary mt-3 align-items-center d-flex'
                    onClick={() => navigate('/addTask')}>
              <FiPlus/>
              <span>Add Task</span>
          </Button>
          <div className="task-list-container mt-4">
              <div className="pending-tasks">
                  {pendingTasks === null ? <>
                      <span className='fs-6'>All Tasks are Completed</span>
                  </> : pendingTasks?.map((todo: Todo) => {
                      return <Task key={todo.id} repository={repository} {...todo}/>
                  })
                  }

              </div>
              <div className="completed-tasks">
                  <UncontrolledAccordion flush className='w-75 mt-4'>
                      <AccordionItem>
                          <AccordionHeader targetId='1'>
                              Show Completed Tasks
                          </AccordionHeader>
                          <AccordionBody accordionId='1'>
                              {completedTasks === null ? <>
                                  <span className='fs-6'>All Tasks are Completed</span>
                              </> : completedTasks?.map((todo: Todo) => {
                                  return <Task key={todo.id} repository={repository} {...todo}/>
                              })
                              }
                          </AccordionBody>
                      </AccordionItem>
                  </UncontrolledAccordion>
              </div>
          </div>
      </>
  )
}