import {useState} from "react";
import {Todo} from "../../Domain/Model/Model";
import {TodoRepository} from "../../Domain/Repository/Repository";
import {addTask, deleteTask, editTask, getTodos, markAsComplete} from "../../Domain/Usecases/Usecases";

export const useTodoModelController = (repository:TodoRepository) => {
    const [todoList, setTodoList] = useState<Todo[]>();

    async function init(){
        let response = await getTodos(repository)
        setTodoList(response)
    }
    const handleAddTask = (name: string) =>{
         addTask(repository, name)
    }

    const handleEditTask = (name:string, id: number) => {
         editTask(repository, id, name)
    }

    const handleDeleteTask = (id: number) => {
        deleteTask(repository, id)
    }

    const handleMarkAsComplete = (id:number) => {
        markAsComplete(repository, id)
    }

    return{
        todoList,
        handleAddTask,
        handleEditTask,
        handleDeleteTask,
        handleMarkAsComplete,
        init
    }
}