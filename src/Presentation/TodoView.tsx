import {Route, Routes} from "react-router-dom";
import type {TodoRepository} from "../Domain/Repository/Repository";
import { AddTask } from "./AddTask/AddTask";
import { EditTask } from "./EditTask/EditTask";
import { SharedLayout } from "./SharedLayout/SharedLayout";
import { TaskPage } from "./TaskPage/TaskPage";
import React from "react";
import {Login} from "./Login/Login";
import {Register} from "./Register/Register";
import {PrivateRoute} from "./PrivateRoute";

type TodoViewProps = {
    repository: TodoRepository,
}
export const TodoView:React.FC<TodoViewProps> = ({repository}) => {
    return (
        <Routes>
            <Route path='/login' element={<Login/>}/>
            <Route path='/register' element={<Register/>}/>
            <Route path='/' element={
                <PrivateRoute>
                    <SharedLayout/>
                </PrivateRoute>
            }>
                <Route index element={<TaskPage repository={repository}/>}></Route>
                <Route path='addTask' element={<AddTask repository={repository}/>}></Route>
                <Route path='editTask' element={<EditTask repository={repository}/>}></Route>
            </Route>
        </Routes>
    );
}