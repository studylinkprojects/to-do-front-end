import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../store";

interface LoginState {
    isLoggedIn: boolean
}

const initialState: LoginState = {
    isLoggedIn: false
}

const loginSlice = createSlice({
    name:'login',
    initialState,
    reducers:{
        setIsLoggedIn: (state, action:PayloadAction<boolean> ) => {
            state.isLoggedIn = action.payload
        }
    }
})

export const {setIsLoggedIn} = loginSlice.actions;
export const isLoggedIn = (state: RootState) => (state.login.isLoggedIn)
export const loginReducer = loginSlice.reducer;