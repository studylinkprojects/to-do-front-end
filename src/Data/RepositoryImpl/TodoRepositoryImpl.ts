import {Todo} from "../../Domain/Model/Model";
import {TodoRepository} from "../../Domain/Repository/Repository";
import {TodoDataSource} from "../DataSource/TodoDataSource";


 export class TodoRepositoryImpl implements TodoRepository{
    dataSource: TodoDataSource
    constructor(DataSource: TodoDataSource) {
        this.dataSource = DataSource
    }
    async getTodos(): Promise<Todo[]> {
        return await this.dataSource.getTodos();
    }

    editTask( id:number, name: string) {
        return this.dataSource.editTask(id, name);
    }
    addTask( name: string) {
        return this.dataSource.addTask(name);
    }

    deleteTask( id: number) {
        return this.dataSource.deleteTask(id);
    }

    markAsComplete( id: number) {
        return this.dataSource.markAsComplete(id);
    }

    login(email: string, password: string) {
        return this.dataSource.login(email,password)
    }

    register(name: string, email: string, password: string) {
        return this.dataSource.register(name, email, password)
    }
 }