import {TodoDataSource} from "../TodoDataSource";
import {Todo} from "../../../Domain/Model/Model";

export class TodoDataSourceImpl implements TodoDataSource{
    async getTodos(): Promise<Todo[]> {
        let response = await fetch('https://jsonplaceholder.typicode.com/todos')
        let data = await response.json()
        return data?.map((todo: any) => ({
            id:todo.id,
            name: todo.title,
            status:todo.completed
        }));
    }

    editTask( id:number, name: string) {

    }

    deleteTask(id: number) {
    }

    markAsComplete( id : number) {
    }

    addTask(name: string) {

    }

    login(email: string, password: string) {

    }

    register(name: string, email: string, password: string) {

    }

}