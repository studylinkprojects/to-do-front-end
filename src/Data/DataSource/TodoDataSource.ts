import {Todo} from "../../Domain/Model/Model";

export interface TodoDataSource {
    getTodos(): Promise<Todo[]>;
    editTask( id:number, name: string):void;
    addTask(name: string):void;
    deleteTask(id:number):void;
    markAsComplete(id: number):void;
    login(email:string, password: string):void
    register(name:string, email:string, password: string):void
}

