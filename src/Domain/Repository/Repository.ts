import {Todo} from "../Model/Model";

export interface TodoRepository {
    getTodos(): Promise<Todo[]>;
    addTask(name:string):void ;
    editTask( id:number, name: string):void;
    deleteTask(id: number):void;
    markAsComplete(id: number):void

    login(email:string, password: string):void
    register(name:string, email:string, password: string):void
}