

export type Todo = {
    id: number,
    name: string,
    status: boolean
}

export type User = {
    name: string,
    email: string,
    password: string
}