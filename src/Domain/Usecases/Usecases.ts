import {TodoRepository} from "../Repository/Repository";

export async function  getTodos(repository: TodoRepository){
    return await repository.getTodos();
}

export function addTask(repository: TodoRepository, name: string) {
    return repository.addTask(name)
}

export function editTask( repository: TodoRepository, id:number, name: string) {
    return repository.editTask(id, name)
}

export function deleteTask(repository: TodoRepository, id:number) {
    return repository.deleteTask(id)
}

export function markAsComplete(repository: TodoRepository, id:number) {
    return repository.deleteTask(id)
}